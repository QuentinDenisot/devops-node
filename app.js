const express       = require('express');
const bodyparser    = require('body-parser');
const db            = require('./database');

const app = express();

app.use(bodyparser.json());

// app.use('/', );

app.listen(3000, () => console.log('Listening'));